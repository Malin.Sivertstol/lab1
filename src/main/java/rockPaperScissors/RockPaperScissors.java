package rockPaperScissors;

import java.util.*;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors

        while (true) {
            System.out.println("Let's play round " + roundCounter);
            Random random = new Random();
            String compChoice = rpsChoices.get(new Random().nextInt(rpsChoices.size()));
            String playerInput = readInput("Your choice (Rock/Paper/Scissors)? ").toLowerCase();

            while (true) {
                if (! rpsChoices.contains(playerInput)){
                    System.out.println("I do not understand " + playerInput + ". Could you try again?");
                }
                else {
                    break;
                }
            }


            if (playerInput.equals(compChoice)) {
                System.out.println("Human chose " + playerInput + ", computer chose " + compChoice + ". It's a tie!");
            } else if ((playerInput.equals("rock")) && (compChoice.equals("paper"))) {
                System.out.println("Human chose rock, computer chose paper. Computer wins!");
                computerScore += 1;
            } else if ((playerInput.equals("rock")) && (compChoice.equals("scissors"))) {
                System.out.println("Human chose rock, computer chose scissors. Human wins!");
                humanScore += 1;
            } else if ((playerInput.equals("paper")) && (compChoice.equals("rock"))) {
                System.out.println("Human chose paper, computer chose rock. Human wins!");
                humanScore += 1;
            } else if ((playerInput.equals("paper")) && (compChoice.equals("scissors"))) {
                System.out.println("Human chose paper, computer chose scissors. Computer wins!");
                computerScore += 1;
            } else if ((playerInput.equals("scissors")) && (compChoice.equals("paper"))) {
                System.out.println("Human chose scissors, computer chose paper. Human wins!");
                humanScore += 1;
            } else if ((playerInput.equals("scissors")) && (compChoice.equals("rock"))) {
                System.out.println("Human chose scissors, computer chose rock. Computer wins!");
                computerScore += 1;
            }

            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            String cont = readInput("Do you wish to continue playing? (y/n)?");
            if (!cont.equals("y")) {

                break;
            }
            else {
                roundCounter += 1;
            }
        }
        System.out.println("Bye bye :)");
    }



    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
